<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

$app = new \Slim\App;

$app->get('/', function (Request $request, Response $response) {
    if (isset($_SERVER["uid"])){
        return "Hello: " . $_SERVER["uid"];
      } else {
        return $response->withStatus(302)->withHeader('Location', 'login');
      }
});

$app->get('/login', function (Request $request, Response $response) {
  return $response->withStatus(302)->withHeader('Location', '/php-shib-example');
});

$app->run();