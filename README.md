# Interfacing PHP and Shibboleth - OleMiss

## Getting Started

This a example assumes that you have a functional and authenticating Shibboleth server established within the University. This can be tested by visiting your servers' Shib instance at ```https://<server>/Shibboleth.sso/Login```. This should redirect you to the login page with the University. After a success login, visiting ```https://<server>/Shibboleth.sso/Session``` should so if you have a valid shib session or not. **The remainder of this example is based on this functionality.**

## Setting up Apache

The Apache config is the crux of making the Shib attributes available to PHP via the $_SERVER super global. The attributes are only available to locations that the Shib module is active for. This can be done with the following:

1. Create an Apache conf in __conf.d__
```
sudo touch /etc/httpd/conf.d/php-shib-example.conf
```
2. Edit the conf to include the following
```
<Location /php-shib-example>
   AuthType Shibboleth
   ShibRequestSetting requireSession false
   Require shibboleth
 </Location>
 <Location /php-shib-example/login>
   AuthType shibboleth
   ShibRequestSetting requireSession 1
   ShibUseEnvironment On
   require shib-session
 </Location>
 ```
 **Note:** This config assumes you are placing the directory "php-shib-example" at the root of your webserver directory
 3. Restart Apache
 ```sudo systemctl restart httpd```

 ## Setting up PHP (via framework)

The attributes being returned by Shibboleth are availbe in the $_SESSION variable based on their name (e.g. $_SERVER["uid"]). I wanted to be able to interact with this data within a PHP framework, so I used my preferred framework [Slim PHP](https://www.slimframework.com) for prototyping. These instructions are meant framework specific, but should help lay the groundwork for other frameworks.

1. Adding Slim PHP with composer

Setting up [Slim PHP](https://www.slimframework.com) is as easy as ```composer require slim/slim``` or, if you've cloned this project, ```composer update```

2. htaccess

Slim requires a __.htaccess__ file to forward all inbound requests to index.php for nice url routing. Unfortunately, for this to function well with the redirects, one line of the normal config is removed (commented out).

```
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
# RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^ index.php [QSA,L]
```

**Note:** The line commented out is for handling directories as part of the nice urls. I don't fully understand how this will affect other functionality down the road...yet.

3. index.php

The __index.php__ contains two routes, the default for path "/" and for "/login".

  * get("/")
  This route is the default for the folder __php-shib-example__ as it appears on my server and it looks to see if the $_SERVER["uid"] is set, which would indicate to the app whether or not someone has logged in via Shibboleth. If it is set, then print it back to the user, if it is not set, redirect the user to "/login".

  * get("/login")
  This route exists for to help the user be navigated to somewhere after a successful log in. I found, during testing, that Apache will handle configuration from the __<Location>__ directive first, and then the webapp. This is why one of the mod_rewrite conditionals had to be commented out. Again, I don't fully understand this at the moment. Once a user successfully signs in via Shibboleth, they are redirected back to their refer url, "/login". This catches that return and then sends them somewhere useful within the app.


4. Test and Verify

Once everything is set up, visiting ```http://<server>/php-shib-example``` should immediate redirect you to the Shibboleth authentication page. Once logged in, you should be redirected back to ```http://<server>/php-shib-example``` and greeted with "Hello: <webID>".

5. Expand

If everything is working for you, CONGRATULATIONS!!!! I hope that you found this little example informative and helps you understand how to interact with Shibboleth via PHP.

### To Do

* Understand the Slim mod_rewrite conditional that was commented out
* Expand the example to include secure sessions
* Expand the example to Laravel

### Special Thanks

Special thanks to DSG and the Sally McDonnell Barksdale Honors College for allowing me to work on this problem.
